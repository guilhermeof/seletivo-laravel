<?php

use App\Models\Empresa;
use Illuminate\Database\Seeder;

Class EmpresasTableSeeder extends Seeder {

    public function run()
    {

        $faker = \Faker\Factory::create('pt_BR');

        for ($i = 0; $i < 30; $i++) {
            $empresa = new Empresa();
            $empresa->razao_social = $faker->company;
            $empresa->inscricao_estadual =$faker->buildingNumber;
            $nome = explode(' ', $empresa->nome);
            $empresa->cnpj = $faker->cnpj(false);
            $empresa->site = $faker->url;
            $empresa->email = $nome[0].'.'.end($nome).$faker->randomNumber(3).'@gmail.com';

            $empresa->enderecos = [
                [
                    'cep' => $faker->postcode,
                    'rua' => $faker->streetAddress,
                    'numero' => $faker->randomNumber(3),
                    'bairro' => $faker->streetName,
                    'complemento' => $faker->secondaryAddress,
                    'cidade' => $faker->city,
                    'estado' => $faker->state
                ]
            ];

            $empresa->telefones = [
                [
                    'ddd' => $faker->areaCode,
                    'numero' => $faker->phoneNumber,
                    'tipo' => 'comercial'
                ]
            ];
            $empresa->save();
        }
    }
}

