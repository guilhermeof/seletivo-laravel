<?php

use App\Models\Cliente;
use Illuminate\Database\Seeder;

Class ClientesTableSeeder extends Seeder {

    public function run()
    {

        $faker = \Faker\Factory::create('pt_BR');

        for ($i = 0; $i < 30; $i++) {
            $cliente = new Cliente();
            $cliente->nome = $faker->firstName.' '.$faker->lastName;
            $nome = explode(' ', $cliente->nome);
            $cliente->cpf = $faker->cpf(false);
            $cliente->data_nascimento = $faker->date('d/m/Y');
            $cliente->nome_mae = $faker->firstNameFemale.' '.$faker->lastName;;
            $cliente->email = $nome[0].'.'.end($nome).$faker->randomNumber(3).'@gmail.com';
            $cliente->senha = bcrypt('1234');

            $cliente->enderecos = [
                [
                    'cep' => $faker->postcode,
                    'rua' => $faker->streetAddress,
                    'numero' => $faker->randomNumber(3),
                    'bairro' => $faker->streetName,
                    'complemento' => $faker->secondaryAddress,
                    'cidade' => $faker->city,
                    'estado' => $faker->state
                ]
            ];

            $cliente->telefones = [
                [
                    'ddd' => $faker->areaCode,
                    'numero' => $faker->phoneNumber,
                    'tipo' => $faker->randomElement(['comercial', 'pessoal'])
                ]
            ];
            $cliente->save();
        }
    }
}

