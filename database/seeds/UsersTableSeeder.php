<?php

use App\Models\User;
use Illuminate\Database\Seeder;

Class UsersTableSeeder extends Seeder {

    public function run()
    {

        User::create([
            'name' => 'Guilherme Oliveira',
            'email' => 'admin@admin.com',
            'password' => bcrypt("1234")
        ]);

    }
}

