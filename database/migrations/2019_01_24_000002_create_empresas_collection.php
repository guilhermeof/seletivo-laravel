<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasCollection extends Migration
{

    protected $connection = 'mongodb';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('empresas', function (Blueprint $collection){
            $collection->index('razao_social');
            $collection->index('inscricao_estadual');
            $collection->text('cnpj');
            $collection->date('site');
            $collection->text('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('empresas', function (Blueprint $collection){
            $collection->drop();
        });
    }
}
