<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesCollection extends Migration
{

    protected $connection = 'mongodb';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('clientes', function (Blueprint $collection){
            $collection->index('nome');
            $collection->text('cpf');
            $collection->date('data_nascimento');
            $collection->text('nome_mae');
            $collection->text('email');
            $collection->text('senha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('clientes', function (Blueprint $collection){
            $collection->drop();
        });
    }
}
