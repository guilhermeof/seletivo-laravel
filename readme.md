Seletivo para vaga remota
=======================

Introdução
------------
Esse é o repositório da api com a problemática proposta.

Problemática
------------

    1 - Transformar os modelos no formato SQL para o estilo de documento do MongoDB
    1.1 - Configurar uma instância do MongoDB
    2 - Criar um projeto Laravel na versão 5.7
    3 - Configurar o projeto para acesso ao modelo NoSQL
    4 - Criar os arquivos de acesso ao banco (Eloquent Models)
    5 - Configurar a autenticação via JWT (tymon/jwt-auth)
    	5.1 - As informações de login do cliente são: email e senha
    6 - Criar uma API com os seguintes endpoints, seguindo o padrão REST:
    	Nova empresa
    	Todas as empresas
    	Edição de informações da empresa
    	Exclusão
          	Novo cliente ( associá-lo a uma empresa)
          	Editar cliente
          	Trocar empresa do cliente
          	Excluir cliente

Tecnologias utilizadas:
-----------------------

 * PHP 7.2.14
 * Laravel Framework 5.7
 * MongoDB v4.0.5


Instalação
------------

Usando composer (recomendado)
----------------------------
Clone o repositório e manualmente execute o 'composer':

    git clone https://gitlab.com/guilhermeof/seletivo-laravel.git
    cd seletivo-laravel
    composer install

Os comandos acima baixam o código do sistema e instalam suas dependências. Agora é preciso configurar o sistema.

Você pode copiar o arquivo .env.example e criar um novo chamado .env, nesse arquivo ficarão as configurações do banco de dados e demais configurações do sistema.

    cp .env.example .env

Abra o arquivo .env e configure-o de acordo com as suas informações.

Por fim execute o comando abaixo parar criar uma chave para a aplicação:

    php artisan key:generate

Executar o comando abaixo para gerar as migrations e os seed de teste.

    php artisan migrate --seed

Executar o comando abaixo para gerar uma chave do JWT para autenticação.

    php artisan jwt:secret

Pronto! Se você seguiu todos os passos corretamente o sistema já está disponível para você. Para fazer login acesse a rota ```POST /api/v1/login``` usando as credenciais abaixo:

    Usuário: admin@admin.com
    Senha: 1234

A API usa o JWT para autenticação, dessa forma os endpoints listados abaixo precisam de um token válido para serem acessados.

### Cliente

Endpoints de clientes.

* Lista todos os clientes cadastrados :
  - Método : `GET`
  - Rota : `/api/v1/clientes/`
* Cria um cliente :
  - Método : `POST`
  - Rota : `/api/v1/clientes/`
* Lista um cliente pelo id :
  - Método : `GET`
  - Rota : `/api/v1/clientes/:id/`
* Atualiza um cliente : `PUT /api/v1/clientes/:id/`
  - Método : `PUT`
  - Rota : `/api/v1/clientes/:id/`
* Remove um cliente :
  - Método : `DELETE`
  - Rota : `/api/v1/clientes/:id/`

### Empresa

Endpoints de empresas.

* Lista todas as empresas cadastradas :
  - Método : `GET`
  - Rota : `/api/v1/empresas/`
* Cria uma empresa :
  - Método : `POST`
  - Rota : `/api/v1/empresas/`
* Lista uma empresa pelo id :
  - Método : `GET`
  - Rota : `/api/v1/empresas/:id`
* Atualiza os dados de uma empresa pelo id :
  - Método : `PUT`
  - Rota : `/api/v1/empresas/:id`
* Remove uma empresa :
  - Método : `DELETE`
  - Rota : `/api/v1/empresas/:id`
* Associa um cliente a uma empresa :
  - Método : `PUT`
  - Rota : `/api/v1/empresas/:id/cliente/:id`
