<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['api'], 'prefix' => 'v1'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });

    Route::group(['prefix' => 'clientes', 'middleware' => 'jwt.verify'], function () {
        Route::get('/', 'ClienteController@index')->name('clientes.index');
        Route::post('/', 'ClienteController@store')->name('clientes.store');
        Route::get('/{clienteid}', 'ClienteController@show')->name('clientes.show');
        Route::put('/{clienteid}', 'ClienteController@update')->name('clientes.update');
        Route::delete('/{clienteid}', 'ClienteController@destroy')->name('clientes.destroy');
    });

    Route::group(['prefix' => 'empresas', 'middleware' => 'jwt.verify'], function () {
        Route::get('/', 'EmpresaController@index')->name('empresas.index');
        Route::post('/', 'EmpresaController@store')->name('empresas.store');
        Route::get('/{empresaid}', 'EmpresaController@show')->name('empresas.show');
        Route::put('/{empresaid}', 'EmpresaController@update')->name('empresas.update');
        Route::put('/{empresaid}/cliente/{clienteid}', 'EmpresaController@clienteEmpresa')->name('empresas.clienteEmpresa');
        Route::delete('/{empresaid}', 'EmpresaController@destroy')->name('empresas.destroy');
    });
});
