<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST') {
            return [
                'nome' => 'required|string|max:255',
                'cpf' => 'required|alpha_num|min:11|max:11',
                'data_nascimento' => 'required|date',
                'email' => 'required|email',
                'nome_mae' => 'required|string|max:255',

                'enderecos' => 'array|nullable',
                'enderecos.*.cep' => 'required|alpha_num',
                'enderecos.*.logradouro' => 'required|string|max:255',
                'enderecos.*.numero' => 'required|numeric',
                'enderecos.*.bairro' => 'required|string|max:100',
                'enderecos.*.complemento' => 'required|string|max:100',
                'enderecos.*.cidade' => 'required|string|max:100',
                'enderecos.*.estado' => 'required|string|min:2|max:50',

                'telefones' => 'array|nullable',
                'telefones.*.ddd' => 'required|alpha_num|max:3',
                'telefones.*.numero' => 'required|alpha_num|min:8|max:15',
                'telefones.*.tipo' => 'required|min:5',
            ];
        }

        return [
            'nome' => 'string|max:50',
            'cpf' => 'alpha_num|min:11|max:11',
            'data_nascimento' => 'date',
            'email' => 'email',
            'nome_mae' => 'string|max:50',

            'enderecos' => 'array|nullable',
            'enderecos.*.cep' => 'required|alpha_num',
            'enderecos.*.logradouro' => 'required|string|max:255',
            'enderecos.*.numero' => 'required|numeric',
            'enderecos.*.bairro' => 'required|string|max:100',
            'enderecos.*.complemento' => 'required|string|max:100',
            'enderecos.*.cidade' => 'required|string|max:100',
            'enderecos.*.estado' => 'required|string|min:2|max:50',

            'telefones' => 'array|nullable',
            'telefones.*.ddd' => 'required|alpha_num|max:3',
            'telefones.*.numero' => 'required|alpha_num|min:8|max:15',
            'telefones.*.tipo' => 'required|min:5',
        ];
    }
}
