<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Empresa;
use App\Http\Requests\EmpresaRequest;

class EmpresaController extends Controller
{
    public function index() {
        try {
            $data = Empresa::all();
            if (!$data->count()){
                return response()->json(['message' =>'Nenhuma empresa cadastrada'], 200);
            }
            return response()->json($data, 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function store(EmpresaRequest $request)
    {
        try {
            $empresa = new Empresa();
            $empresa->fill($request->all());
            $empresa->save();

            return response()->json($empresa, 201);

        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function show($empresaId) {
        try {
            $empresa = Empresa::find($empresaId);
            if(!$empresa){
                return response()->json(['message' => 'Nenhuma empresa encontrada com esse id'], 404);
            }
            return response()->json($empresa, 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function update(EmpresaRequest $request, $empresaId)
    {
        try {
            $empresa = Empresa::find($empresaId);
            if(!$empresa){
                return response()->json(['message' => 'Nenhuma empresa encontrada com esse id'], 404);
            }
            $empresa->fill($request->all());
            $empresa->save();
            return response()->json($empresa, 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function clienteEmpresa(Empresa $empresa, Cliente $cliente)
    {
        try {
            $associacoes = $empresa->clientes ? $empresa->clientes : [];
            $associacoes[] = $cliente->getKey();
            $empresa->clientes = array_unique($associacoes);
            $empresa->save();
            return response()->json($empresa, 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function destroy($empresaId)
    {
        try {
            $empresa = Empresa::find($empresaId);
            if(!$empresa){
                return response()->json(['message' => 'Nenhuma empresa encontrada com esse id'], 404);
            }
            $empresa->delete();
            return response()->json(['message' => 'Empresa excluída com sucesso!'], 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }
}