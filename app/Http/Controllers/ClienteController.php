<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Http\Requests\ClienteRequest;
use App\Models\Empresa;


class ClienteController extends Controller
{
    public function index() {
        try {
            $data = Cliente::all();
            if (!$data->count()){
                return response()->json(['message' =>'Nenhum cliente cadastrado'], 200);
            }
            return response()->json($data, 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function store(ClienteRequest $request)
    {
        try {
            $cliente = new Cliente();
            $cliente->fill($request->all());
            $cliente->save();

            return response()->json($cliente, 201);

        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function show($clienteId) {
        try {
            $cliente = Cliente::find($clienteId);
            if(!$cliente){
                return response()->json(['message' => 'Nenhum cliente encontrado com esse id'], 404);
            }
            return response()->json($cliente, 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function update(ClienteRequest $request, $clienteId)
    {
        try {
            $cliente = Cliente::find($clienteId);
            if(!$cliente){
                return response()->json(['message' => 'Nenhum cliente encontrado com esse id'], 404);
            }
            $cliente->fill($request->all());
            $cliente->save();
            return response()->json($cliente, 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function clienteEmpresa(Cliente $cliente, Empresa $empresa)
    {
        try {
            $associacoes = $empresa->clientes ? $empresa->clientes : [];
            $associacoes[] = $cliente->getKey();
            $empresa->clientes = array_unique($associacoes);
            $empresa->save();
            return response()->json($empresa, 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }

    public function destroy($clienteId)
    {
        try {
            $cliente = Cliente::find($clienteId);
            if(!$cliente){
                return response()->json(['message' => 'Nenhum cliente encontrado com esse id'], 404);
            }
            $cliente->delete();
            return response()->json(['message' => 'Cliente excluído com sucesso!'], 200);
        } catch (\Exception $exception)  {
            return response()->json([
                'error' => $exception->getMessage()
            ], 500);
        }
    }
}
