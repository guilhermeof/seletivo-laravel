<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return new JsonResponse(['message' => 'Token is Invalid'], JsonResponse::HTTP_UNAUTHORIZED);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return new JsonResponse(['message' => 'Token is Expired'], JsonResponse::HTTP_UNAUTHORIZED);
            } else {
                return new JsonResponse(['message' => 'Authorization Token not found'], JsonResponse::HTTP_UNAUTHORIZED);
            }
        }
        return $next($request);
    }
}
