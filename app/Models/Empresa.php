<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Empresa extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'empresas';

    protected $fillable = [
        'razao_social',
        'inscricao_estadual',
        'cnpj',
        'site',
        'email',
        'enderecos',
        'telefones',
    ];
}
